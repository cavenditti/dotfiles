-- Shorten function name
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("", "<C-h>", "<cmd>TmuxNavigateLeft<CR>", opts)
keymap("", "<C-j>", "<cmd>TmuxNavigateDown<CR>", opts)
keymap("", "<C-l>", "<cmd>TmuxNavigateRight<CR>", opts)
keymap("", "<C-k>", "<cmd>TmuxNavigateUp<CR>", opts)

-- Resize with arrows
keymap("n", "<C-Up>", "<cmd>resize -2<CR>", opts)
keymap("n", "<C-Down>", "<cmd>resize +2<CR>", opts)
keymap("n", "<C-Left>", "<cmd>vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", "<cmd>vertical resize +2<CR>", opts)

-- Navigate buffers
-- keymap("n", "<S-l>", "<cmd>bnext<CR>", opts)
-- keymap("n", "<S-h>", "<cmd>bprevious<CR>", opts)

-- Clear highlights
keymap("n", "<leader>h", "<cmd>nohlsearch<CR>", opts)

-- Close buffers
keymap("n", "<S-q>", "<cmd>bdelete!<CR>", opts)

-- Better paste
keymap("v", "p", '"_dP', opts)

-- Insert --
-- Press jk fast to enter
keymap("i", "jk", "<ESC>", opts)
keymap("i", "kj", "<ESC>", opts)

-- Visual --
-- Stay in indent mode
-- keymap("v", "<", "<gv", opts)
-- keymap("v", ">", ">gv", opts)

-- Plugins --

-- NeoTree
keymap("n", "<leader>e", "<cmd>Neotree toggle<CR>", opts)
keymap("n", "<leader>se", "<cmd>Neotree toggle<CR>", opts)

-- FZF
keymap("n", "<leader>f", "<cmd>Telescope git_files<CR>", opts)
keymap("n", "<leader>F", "<cmd>Telescope find_files<CR>", opts)
keymap("n", "<leader>r", "<cmd>Telescope live_grep<CR>", opts)
keymap("n", "<leader>b", "<cmd>Telescope buffers<CR>", opts)

--keymap("n", "<leader>h", "<cmd>History<CR>", opts)
keymap("n", "<leader>t", "<cmd>Telescope tags<CR>", opts)
-- keymap("n", "<leader>T", "<cmd>BTags<CR>", opts)
keymap("n", "<leader>R", "<cmd>Telescope registers<CR>", opts)
keymap("n", "<leader>c", "<cmd>Telescope commands<CR>", opts)
keymap("n", "<leader><cmd>", "<cmd>Telescope command_history<CR>", opts)
keymap("n", "<leader>;", "<cmd>Telescope command_history<CR>", opts)
keymap("n", "<leader>/", "<cmd>Telescope search_history<CR>", opts)
keymap("n", "<leader>ls", "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>", opts)
keymap("n", "<leader>lr", "<cmd>Telescope lsp_references<CR>", opts)
keymap("n", "<leader>ld", "<cmd>Telescope lsp_definitions<CR>", opts)
-- diagnostics
keymap("n", "<leader>lp", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts)
keymap("n", "<leader>ln", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts)
keymap("n", "<leader>lb", "<cmd>Lspsaga show_buf_diagnostics<CR>", opts)
keymap("n", "<leader>lw", "<cmd>Lspsaga show_workspace_diagnostics<CR>", opts)

-- Git
keymap("n", "<leader>gg", "<cmd>lua _LAZYGIT_TOGGLE()<CR>", opts)

-- Comment
keymap("n", "<leader>a/", "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", opts)
keymap("x", "<leader>a/", '<ESC><CMD>lua require("Comment.api").toggle_linewise_op(vim.fn.visualmode())<CR>')

-- DAP
keymap("n", "<leader>db", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", opts)
keymap("n", "<leader>dc", "<cmd>lua require'dap'.continue()<cr>", opts)
keymap("n", "<leader>di", "<cmd>lua require'dap'.step_into()<cr>", opts)
keymap("n", "<leader>do", "<cmd>lua require'dap'.step_over()<cr>", opts)
keymap("n", "<leader>dO", "<cmd>lua require'dap'.step_out()<cr>", opts)
keymap("n", "<leader>dr", "<cmd>lua require'dap'.repl.toggle()<cr>", opts)
keymap("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>", opts)
keymap("n", "<leader>du", "<cmd>lua require'dapui'.toggle()<cr>", opts)
keymap("n", "<leader>dt", "<cmd>lua require'dap'.terminate()<cr>", opts)

-- Code action
keymap("n", "<leader>ac", "<cmd>Lspsaga code_action<CR>")

-- rename
keymap("n", "<leader>arr", "<cmd>Lspsaga rename<CR>")
keymap("n", "<leader>arp", "<cmd>Lspsaga project_replace<CR>")

-- Other LSP-related
-- These were in handlers.lua, but they were only being loaded if we had loaded lspconfig, which might not be the case
--   (e.g. Rust file using rustacenvim).

keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
keymap("n", "gI", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
keymap("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
keymap("n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
keymap("n", "<leader>af", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
keymap("n", "<leader>ai", "<cmd>LspInfo<CR>", opts)
keymap("n", "<leader>aI", "<cmd>LspInstallInfo<CR>", opts)
keymap("n", "<leader>aa", "<cmd>lua vim.lsp.buf.code_action()<cr>", opts)
keymap("n", "<leader>aj", "<cmd>lua vim.diagnostic.goto_next({buffer=0})<CR>", opts)
keymap("n", "<leader>ak", "<cmd>lua vim.diagnostic.goto_prev({buffer=0})<CR>", opts)
keymap("n", "<leader>ar", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
keymap("n", "<leader>ss", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
keymap("n", "<leader>aq", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
keymap("n", "<leader>sd", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
-- From the old config

-- Save buffer
keymap("n", "<leader>w", "<cmd>w<CR>", opts)
keymap("n", "<leader>p", "<C-^>", opts)

-- relative numbers
keymap("n", "<leader>srn", "<cmd>set rnu!<CR>", opts)

-- remove extra spaces
keymap("n", "<leader>aR", "<cmd>%s/\\s\\+\\%#\\@<!$//e<CR>", opts)

-- show Undotree
keymap("n", "<leader>su", "<cmd>UndotreeToggle<CR>", opts)
-- show symbols outline
keymap("n", "<leader>so", "<cmd>Lspsaga outline<CR>", opts)

keymap("n", "gn", "<cmd>tabnew<CR>", opts)


-- motions using hop
keymap("n", "<leader><leader>a", "<cmd>lua require'hop'.hint_words()<CR>", opts)
keymap("n", "<leader><leader>A", "<cmd>lua require'hop'.hint_char2()<CR>", opts)
keymap(
  "n",
  "<leader><leader>f",
  "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>F",
  "<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>w",
  "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>b",
  "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>W",
  "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>B",
  "<cmd>lua require'hop'.hint_words({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>k",
  "<cmd>lua require'hop'.hint_lines({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>j",
  "<cmd>lua require'hop'.hint_lines({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>K",
  "<cmd>lua require'hop'.hint_char2({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR})<CR>",
  opts
)
keymap(
  "n",
  "<leader><leader>J",
  "<cmd>lua require'hop'.hint_char2({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR})<CR>",
  opts
)

-- REMAPS
-- Close Neotree then exits on ZX, keep ZZ for closing windows
keymap("n", "ZX", "<cmd>Neotree close<CR><cmd>wq<CR>", opts)


-- Keymaps documentation
local status_ok, wk = pcall(require, "which-key")
if status_ok then
  wk.setup()
  wk.add({
    { "<leader>a",  group = "+actions" },
    { "<leader>a/", desc = "Comment line" },
    { "<leader>af", desc = "Format code" },
    { "<leader>ac", desc = "Code action" },
    { "<leader>d",  group = "+debug" },
    { "<leader>s",  group = "+show" },
    { "<leader>so", desc = "Show symbols outline" },
    { "<leader>su", desc = "Show undotree" },
  })
end
