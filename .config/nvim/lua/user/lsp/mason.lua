local mason_ok, mason = pcall(require, "mason")
if not mason_ok then
  return
end

mason.setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})

local status_ok, mason_lspconfig = pcall(require, "mason-lspconfig")
if not status_ok then
  return
end

local servers = {
  "lua_ls",
  "pbls",
  "clangd",
  "cssls",
  "html",
  "ts_ls",
  "taplo",
  "ruff",
  --"pylsp",
  "pyright",
  "bashls",
  "jsonls",
  "yamlls",
  "sqlls",
  -- "rust_analyzer", -- ~~rust-tools~~ rustaceanvim now takes care of this
}

mason_lspconfig.setup({
  ensure_installed = servers,
  automatic_installation = true
})

--lsp_installer.setup()

local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status_ok then
  return
end

local opts = {}

for _, server in pairs(servers) do
  opts = {
    on_attach = require("user.lsp.handlers").on_attach,
    capabilities = require("user.lsp.handlers").capabilities,
  }

  if server == "sumneko_lua" then
    local sumneko_opts = require "user.lsp.settings.sumneko_lua"
    opts = vim.tbl_deep_extend("force", sumneko_opts, opts)
  end

  if server == "pyright" then
    local pyright_opts = require "user.lsp.settings.pyright"
    opts = vim.tbl_deep_extend("force", pyright_opts, opts)
  end

  lspconfig[server].setup(opts)
end
