--require "user.neo-tree"

-- Automatically install lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Use a protected call so we don't error out on first use
local status_ok, lazy_nvim = pcall(require, "lazy")
if not status_ok then
  return
end

-- Install plugins
return lazy_nvim.setup({
    -- Straight from LunarVim/nvim-basic-ide
    "nvim-lua/plenary.nvim", -- Useful lua functions used by lots of plugins
    -- "windwp/nvim-autopairs", -- Autopairs, integrates with both cmp and treesitter
    "numToStr/Comment.nvim",
    "JoosepAlviste/nvim-ts-context-commentstring",
    "nvim-tree/nvim-web-devicons",
    {
      "nvim-neo-tree/neo-tree.nvim",
      branch = "v3.x",
      lazy = false,
      dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
        "MunifTanjim/nui.nvim",
        -- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
      }
      --config = NeoTreeConfig
    },

    --  "akinsho/bufferline.nvim",
    -- "moll/vim-bbye",
    "nvim-lualine/lualine.nvim",
    -- "akinsho/toggleterm.nvim", -- we have tmux, why use this?
    -- "ahmedkhalf/project.nvim",
    "lukas-reineke/indent-blankline.nvim",
    "goolord/alpha-nvim",

    -- I tend to forget my keymaps
    {
      "folke/which-key.nvim",
      event = "VeryLazy",
      init = function()
        vim.o.timeout = true
        vim.o.timeoutlen = 300
      end,
      opts = {}
    },

    -- Colorschemes
    {
      "folke/tokyonight.nvim",
      lazy = false,
      priority = 1000,
      opts = {},
    },
    -- Eye candy
    "stevearc/dressing.nvim",
    "nvim-telescope/telescope-ui-select.nvim",
    {
      "vigoux/notifier.nvim",
      config = function()
        require("notifier").setup({
          -- You configuration here
        })
      end
    },
    --"j-hui/fidget.nvim", -- monitor LSP tasks status

    -- cmp plugins
    "hrsh7th/nvim-cmp",         -- The completion plugin
    "hrsh7th/cmp-buffer",       -- buffer completions
    "hrsh7th/cmp-path",         -- path completions
    "saadparwaiz1/cmp_luasnip", -- snippet completions
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lua",

    -- snippets
    "L3MON4D3/LuaSnip",             --snippet engine
    "rafamadriz/friendly-snippets", -- a bunch of snippets to use

    -- LSP
    "williamboman/mason.nvim",           -- simple to use language server installer, replaces nvim-lsp-installer
    "williamboman/mason-lspconfig.nvim", -- we need this
    "neovim/nvim-lspconfig",             -- enable LSP
    {
      "mhartington/formatter.nvim",      -- for formatters, replaces null-ls
      config = function()
        -- Utilities for creating configurations
        local util = require "formatter.util"

        -- Provides the Format, FormatWrite, FormatLock, and FormatWriteLock commands
        require("formatter").setup {
          -- Enable or disable logging
          logging = true,
          -- Set the log level
          log_level = vim.log.levels.WARN,
          -- All formatter configurations are opt-in
          filetype = {
            -- Formatter configurations for filetype "lua" go here
            -- and will be executed in order
            lua = {
              -- "formatter.filetypes.lua" defines default configurations for the
              -- "lua" filetype
              require("formatter.filetypes.lua").stylua,

              -- You can also define your own configuration
              function()
                -- Supports conditional formatting
                if util.get_current_buffer_file_name() == "special.lua" then
                  return nil
                end

                -- Full specification of configurations is down below and in Vim help
                -- files
                return {
                  exe = "stylua",
                  args = {
                    "--search-parent-directories",
                    "--stdin-filepath",
                    util.escape_path(util.get_current_buffer_file_path()),
                    "--",
                    "-",
                  },
                  stdin = true,
                }
              end
            },

            -- Use the special "*" filetype for defining formatter configurations on
            -- any filetype
            ["*"] = {
              -- "formatter.filetypes.any" defines default configurations for any
              -- filetype
              require("formatter.filetypes.any").remove_trailing_whitespace
            }
          }
        }
      end
    },
    {
      "mfussenegger/nvim-lint", -- for linters, replaces null-ls
      config = function()
        vim.api.nvim_create_autocmd({ "BufWritePost" }, {
          callback = function()
            require("lint").try_lint()
          end,
        })
      end
    },
    "RRethy/vim-illuminate",

    --  need an alternative to 'kosayoda/nvim-lightbulb
    {
      "nvimdev/lspsaga.nvim",
      event = 'LspAttach',
      opts = {},
      dependencies = {
        'nvim-treesitter/nvim-treesitter',
        'nvim-tree/nvim-web-devicons',
      }
    },
    {
      "folke/trouble.nvim",
      dependencies = "nvim-tree/nvim-web-devicons",
      opts = {}
    },

    -- Telescope - so much faster now
    "nvim-telescope/telescope.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build =
      "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build"
    },

    -- Treesitter
    "nvim-treesitter/nvim-treesitter",

    -- Supercharged macros
    --			"ecthelionvi/NeoComposer.nvim",
    --		dependencies = { "kkharji/sqlite.lua" },
    --	})

    -- docstrings & co.
    {
      "danymat/neogen",
      config = function()
        require("neogen").setup({})
      end,
      dependencies = "nvim-treesitter/nvim-treesitter",
      -- Uncomment next line if you want to follow only stable versions
      -- tag = "*"
    },
    -- Git
    "lewis6991/gitsigns.nvim",
    "tpope/vim-fugitive",
    {
      "sindrets/diffview.nvim",
      dependencies = "nvim-lua/plenary.nvim"
    },

    -- DAP
    "mfussenegger/nvim-dap",
    "rcarriga/nvim-dap-ui",
    "ravenxrz/DAPInstall.nvim",

    -- Session management
    "tpope/vim-obsession",
    {
      "dhruvasagar/vim-prosession",
      dependencies = "tpope/vim-obsession",
      lazy = false
    },

    -- tmux integration
    { "christoomey/vim-tmux-navigator", lazy = false },

    -- easymotion, but better
    {
      'smoka7/hop.nvim',
      version = "*",
      opts = {},
    },

    -- undotree and outline
    {
      "mbbill/undotree",
      lazy = false
    },

    {
      "folke/trouble.nvim",
      opts = {},
      cmd = "Trouble",
      keys = {
        {
          "<leader>xx",
          "<cmd>Trouble diagnostics toggle<cr>",
          desc = "Diagnostics (Trouble)",
        },
        {
          "<leader>xX",
          "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
          desc = "Buffer Diagnostics (Trouble)",
        },
        {
          "<leader>cs",
          "<cmd>Trouble symbols toggle focus=false<cr>",
          desc = "Symbols (Trouble)",
        },
        {
          "<leader>cl",
          "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
          desc = "LSP Definitions / references / ... (Trouble)",
        },
        {
          "<leader>xL",
          "<cmd>Trouble loclist toggle<cr>",
          desc = "Location List (Trouble)",
        },
        {
          "<leader>xQ",
          "<cmd>Trouble qflist toggle<cr>",
          desc = "Quickfix List (Trouble)",
        },
      },
    },


    "cavenditti/vim-slash",

    -- various ui tweaks
    {
      "folke/noice.nvim",
      event = "VeryLazy",
      opts = {
        -- add any options here
      },
      config = function()
        require("noice").setup({
          lsp = {
            -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
            override = {
              ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
              ["vim.lsp.util.stylize_markdown"] = true,
              ["cmp.entry.get_documentation"] = true, -- requires hrsh7th/nvim-cmp
            },
          },
          messages = {
            view = "mini",               -- default view for messages
            view_error = "mini",         -- view for errors
            view_warn = "mini",          -- view for warnings
            view_history = "messages",   -- view for :messages
            view_search = "virtualtext", -- view for search count messages. Set to `false` to disable
          },
          presets = {
            bottom_search = true,         -- use a classic bottom cmdline for search
            command_palette = true,       -- position the cmdline and popupmenu together
            long_message_to_split = true, -- long messages will be sent to a split
            inc_rename = false,           -- enables an input dialog for inc-rename.nvim
            lsp_doc_border = false,       -- add a border to hover docs and signature help
          },
          cmdline = {
            view = "cmdline"
          },
          notify = {
            enabled = true,
            view = "mini",
          },
        })
      end,
      dependencies = {
        "MunifTanjim/nui.nvim",
        "rcarriga/nvim-notify",
      }
    },

    {
      "gbprod/stay-in-place.nvim",
      config = function()
        require("stay-in-place").setup({})
      end
    },

    -- rust-tools
    {
      'mrcjkb/rustaceanvim',
      version = '^4', -- Recommended
      lazy = false,   -- This plugin is already lazy
      config = function()
        vim.g.rustaceanvim = {
          -- Plugin configuration
          -- tools = {},
          -- LSP configuration
          server = {
            on_attach = function(client, bufnr)
              -- you can also put keymaps in here
              vim.lsp.inlay_hint.enable(true)
            end,
            settings = {
              -- rust-analyzer language server configuration
              ["rust-analyzer"] = {},
            },
            --   },
            --   -- DAP configuration
            --   dap = {},
          },
        }
      end
    },

    -- markdown preview
    {
      "iamcco/markdown-preview.nvim",
      build = "cd app && npm install",
      setup = function()
        vim.g.mkdp_filetypes = { "markdown" }
      end,
      ft = { "markdown" }
    },

    -- orgmode
    --"nvim-orgmode/orgmode",

    -- No distractions
    --{
      --"folke/zen-mode.nvim",
      --opts = {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
      --}
    --},

    -- copilot
    --[[
    {
      "github/copilot.vim",
      cmd = "Copilot",
    },
    ]] --
    -- alternative full-lua implementation
    {
      "zbirenbaum/copilot.lua",
      cmd = "Copilot",
      event = "InsertEnter",
      build = ":Copilot auth",
      opts = {
        suggestion = { enabled = true },
        panel = { enabled = false },
      },
    },
  },
  {
    defaults = {
      lazy = true, -- should plugins be lazy-loaded?
    }
  }
)
