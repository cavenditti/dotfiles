export EDITOR=nvim
fzf_key_bindings

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
#eval /opt/miniconda3/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<

# custom functions
function ggb
  cd (g top)
  commandline -f repaint
end

bind \eG ggb

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin $PATH /home/carlo/.ghcup/bin # ghcup-env

status is-interactive; and begin
    set fish_tmux_autostart true
end
