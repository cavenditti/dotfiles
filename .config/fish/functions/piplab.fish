function piplab --wraps='pipenv install jupyterlab ipykernel matplotlib pandas && pipenv run jupyter lab' --description 'alias piplab pipenv install jupyterlab ipykernel matplotlib pandas && pipenv run jupyter lab'
  pipenv install jupyterlab ipykernel matplotlib pandas && pipenv run jupyter lab $argv; 
end
