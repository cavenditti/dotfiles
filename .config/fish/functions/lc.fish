function lc --description 'show content of directories and files'
if test -d $argv[1]
ls -lavF $argv[1]
else
less $argv[1]
end

end
