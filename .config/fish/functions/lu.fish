function lu --wraps='broot --sort-by-size' --description 'alias lu broot --sort-by-size'
  broot --sort-by-size $argv; 
end
