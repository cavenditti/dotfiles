function gl --wraps=echo\ test\ \&\&\ g\ lg\ -8\ test\ \&\&\ echo\ \\nmaster\ \&\&\ g\ lg\ -8\ master --description alias\ gl\ echo\ test\ \&\&\ g\ lg\ -8\ test\ \&\&\ echo\ \\nmaster\ \&\&\ g\ lg\ -8\ master
  echo test && g lg -8 test && echo \nmaster && g lg -8 master $argv; 
end
