# Defined in - @ line 1
function gg --wraps='broot --conf ~/.config/broot/git-diff-conf.toml --git-status' --description 'alias gg broot --conf ~/.config/broot/git-diff-conf.toml --git-status'
  broot --conf ~/.config/broot/git-diff-conf.toml --git-status $argv;
end
