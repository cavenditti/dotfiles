"" General
set t_Co=256

set number	" Show line numbers
set linebreak	" Break lines at word (requires Wrap lines)
set showbreak=>\   	" Wrap-broken line prefix
set textwidth=120	" Line wrap (number of cols)
set showmatch	" Highlight matching brace
set visualbell	" Use visual bell (no beeping)
set cursorline	" Hightlight current line

set splitright
set splitbelow

set hlsearch	" Highlight all search results
set smartcase	" Enable smart-case search
set ignorecase	" Always case-insensitive
set wildignorecase
set incsearch	" Searches for strings incrementally

set autoindent	" Auto-indent new lines
set shiftwidth=4	" Number of auto-indent spaces
set smartindent	" Enable smart-indent
set smarttab	" Enable smart-tabs
set softtabstop=4	" Number of spaces per Tab

set path+=**
set wildmenu
set wildmode=list:longest,full
"set wildmode=full

"" Advanced
set ruler	" Show row and column ruler information

set hidden	" We're always keep the session anyway
set undofile
set undodir=~/.vim/undo/

set history=5000
set undolevels=1500	" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

set encoding=utf-8

set clipboard=unnamedplus

" folding
set foldmethod=indent
set foldlevel=99


" Hightlight unwanted spaces
highlight ExtraWhitespace ctermbg=red guibg=red
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+\%#\@<!$/

""" netrw files
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
"augroup ProjectDrawer
"    autocmd!
"    autocmd VimEnter * :Vexplore
"augroup END

"nnoremap <Leader>w :Vexplore<CR>

""" Commands
com Q q
"toggle relative line numbers
com RelativeNumbers set rnu!
com RN set rnu!
com RemoveExtraSpaces :%s/\s\+\%#\@<!$//e
com Vbuffer :vert sb<space>

""" Keybindings

" Leader
let mapleader = " "

nnoremap <Leader>p <C-^><CR>
nnoremap <Leader>w :w<CR>

nnoremap <Leader>di :display<CR>
"nnoremap s Paste from "0?

" Split navigation
"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

" fzf shortcuts
nnoremap <Leader>b :Buffers<CR>
nnoremap <Leader>f :Files<CR>
nnoremap <Leader>F :Files<CR>
nnoremap <Leader>h :History<CR>
nnoremap <Leader>T :Tags<CR>
nnoremap <Leader>t :BTags<CR>
nnoremap <Leader>c :Commands<CR>
nnoremap <Leader>m :Marks<CR>
nnoremap <Leader>: :History:<CR>
nnoremap <Leader>; :History:<CR>
nnoremap <Leader>/ :History/<CR>

" double leader is used by easymotion and we'll keep it that way
" nnoremap <Leader><Leader> :Lines<CR>
nnoremap <Leader><CR> :Lines<CR>

" Tabs
nnoremap gn :tabnew

" Highlight
"nnoremap <silent> <Leader>hl :exe "let m = matchadd('WildMenu','\\%" . line('.') . "l')"<CR>
"nnoremap <silent> <Leader>hw :exe "let m=matchadd('WildMenu','\\<\\w*\\%" . line(".") . "l\\%" . col(".") . "c\\w*\\>')"<CR>
"nnoremap <silent> <Leader><CR> :call clearmatches()<CR>:set hlsearch!<CR>
nnoremap <silent> <Leader>hl :set hlsearch!<CR>

let s:hidden_bar = 0
function! ToggleStatusline()
    if s:hidden_bar  == 0
        let s:hidden_bar = 1
        set laststatus=1
    else
        let s:hidden_bar = 0
        set laststatus=2
    endif
endfunction

com Statusline :call ToggleStatusline()

nnoremap <S-h> :call ToggleHiddenAll()<CR>

" search for selected text.
let s:save_cpo = &cpo | set cpo&vim
if !exists('g:VeryLiteral')
    let g:VeryLiteral = 0
endif
function! s:VSetSearch(cmd)
    let old_reg = getreg('"')
    let old_regtype = getregtype('"')
    normal! gvy
    if @@ =~? '^[0-9a-z,_]*$' || @@ =~? '^[0-9a-z ,_]*$' && g:VeryLiteral
	let @/ = @@
    else
	let pat = escape(@@, a:cmd.'\')
	if g:VeryLiteral
	    let pat = substitute(pat, '\n', '\\n', 'g')
	else
	    let pat = substitute(pat, '^\_s\+', '\\s\\+', '')
	    let pat = substitute(pat, '\_s\+$', '\\s\\*', '')
	    let pat = substitute(pat, '\_s\+', '\\_s\\+', 'g')
	endif
	let @/ = '\V'.pat
    endif
    normal! gV
    call setreg('"', old_reg, old_regtype)
endfunction
vnoremap <silent> * :<C-U>call <SID>VSetSearch('/')<CR>/<C-R>/<CR>
vnoremap <silent> # :<C-U>call <SID>VSetSearch('?')<CR>?<C-R>/<CR>
vmap <kMultiply> *
nmap <silent> <Plug>VLToggle :let g:VeryLiteral = !g:VeryLiteral
	    \\| echo "VeryLiteral " . (g:VeryLiteral ? "On" : "Off")<CR>
if !hasmapto("<Plug>VLToggle")
    nmap <unique> <Leader>vl <Plug>VLToggle
endif
let &cpo = s:save_cpo | unlet s:save_cpo


""" Terminal

set termwinkey=<C-S>

" consistent movement across splits
" TODO disable when using fzf
tnoremap <C-J> <C-S><C-J>
tnoremap <C-K> <C-S><C-K>
tnoremap <C-L> <C-S><C-L>
tnoremap <C-H> <C-S><C-H>


""" Hex editor
" ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function ToggleHex()
  " hex mode should be considered a read-only operation
  " save values for modified and read-only for restoration later,
  " and clear the read-only flag for now
  let l:modified=&mod
  let l:oldreadonly=&readonly
  let &readonly=0
  let l:oldmodifiable=&modifiable
  let &modifiable=1
  if !exists("b:editHex") || !b:editHex
    " save old options
    let b:oldft=&ft
    let b:oldbin=&bin
    " set new options
    setlocal binary " make sure it overrides any textwidth, etc.
    silent :e " this will reload the file without trickeries
              "(DOS line endings will be shown entirely )
    let &ft="xxd"
    " set status
    let b:editHex=1
    " switch to hex editor
    %!xxd
  else
    " restore old options
    let &ft=b:oldft
    if !b:oldbin
      setlocal nobinary
    endif
    " set status
    let b:editHex=0
    " return to normal editing
    %!xxd -r
  endif
  " restore values for modified and read only state
  let &mod=l:modified
  let &readonly=l:oldreadonly
  let &modifiable=l:oldmodifiable
endfunction

nnoremap <C-X> :Hexmode<CR>
inoremap <C-X> <Esc>:Hexmode<CR>
vnoremap <C-X> :<C-U>Hexmode<CR>


""" If missing get vim plug
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

""" Plugins
call plug#begin('~/.vim/bundle')

" Generic editing
Plug 'junegunn/vim-easy-align'
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

"Plug 'jeffkreeftmeijer/vim-numbertoggle'


" Features
"Plug 'kana/vim-arpeggio' " This is cool but I'll keep messing up, so bye for now
"Arpeggioinoremap jk <Esc>
"Arpeggioinoremap ln <Esc>:RN<CR>

Plug 'benmills/vimux'
map <Leader>rp :VimuxPromptCommand<CR>
map <Leader>rl :VimuxRunLastCommand<CR>
map <Leader>rr :VimuxRunLastCommand<CR>
map <Leader>ri :VimuxInspectRunner<CR>
map <Leader>rc :VimuxCloseRunner<CR>
map <Leader>rs :VimuxInterruptRunner<CR>

map <Leader>re :call VimuxRunCommand("clear; ls; echo; pwd; read -n 1 -p \" \"")<CR>


Plug 'christoomey/vim-tmux-navigator'

let g:tmux_navigator_no_mappings = 1

nnoremap <silent> <C-H> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-J> :TmuxNavigateDown<cr>
nnoremap <silent> <C-K> :TmuxNavigateUp<cr>
nnoremap <silent> <C-L> :TmuxNavigateRight<cr>
"nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>

" Finders
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

function! RipgrepFzf(query, fullscreen)
    let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
    let initial_command = printf(command_fmt, shellescape(a:query))
    let reload_command = printf(command_fmt, '{q}')
    let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
    call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)
nnoremap <Leader>r :RG<CR>

" Search
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'

map z/ <Plug>(incsearch-fuzzy-/)
map z? <Plug>(incsearch-fuzzy-?)
map zg/ <Plug>(incsearch-fuzzy-stay)

" Autocomplete
Plug 'ycm-core/YouCompleteMe'
nnoremap <Leader>d :YcmCompleter GoToDeclaration<CR>
nnoremap <Leader>D :YcmCompleter GoToDefinition<CR>

" Linter
Plug 'dense-analysis/ale'
let g:ale_fixers = ['autopep8', 'yapf']

" TeX and markup
Plug 'lervag/vimtex'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/goyo.vim'

"nmap <Leader>l <Plug>(Limelight)
"xmap <Leader>l <Plug>(Limelight)
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" Session
Plug 'tpope/vim-obsession'
Plug 'dhruvasagar/vim-prosession'

" Minor editor behaviour changes
Plug 'pbrisbin/vim-mkdir' " automatically create directories when saving
Plug 'vim-scripts/ZoomWin'
Plug 'inside/vim-search-pulse'

" Easymotion
Plug 'easymotion/vim-easymotion'
Plug 'haya14busa/incsearch-easymotion.vim'
function! s:config_easyfuzzymotion(...) abort
    return extend(copy({
		\   'converters': [incsearch#config#fuzzyword#converter()],
		\   'modules': [incsearch#config#easymotion#module({'overwin': 1})],
		\   'keymap': {"\<CR>": '<Over>(easymotion)'},
		\   'is_expr': 0,
		\   'is_stay': 1
		\ }), get(a:, 1, {}))
endfunction

noremap <silent><expr> <Leader>j incsearch#go(<SID>config_easyfuzzymotion())

" Git
Plug 'tpope/vim-fugitive'

" Let's give these a try
Plug 'mbbill/undotree'
nmap <Leader>u :UndotreeToggle<CR>

Plug 'preservim/tagbar'
nmap <Leader>o :TagbarToggle<CR>

Plug 'sheerun/vim-polyglot'

"" Language specific
"  Python
Plug 'jeetsukumaran/vim-pythonsense'
let g:is_pythonsense_alternate_motion_keymaps = 1
" This uses doq, the makefile just invokes pip to install it
Plug 'heavenshell/vim-pydocstring', { 'do': 'make install' }
nmap <silent> <C-_> <Plug>(pydocstring)

" format css
com CSSFormat :%s/[{;}]/&\r/g|norm! =gg

" Colorscheme
Plug 'arzg/vim-colors-xcode'
"Plug 'sonph/onehalf', {'rtp': 'vim/'}

""" Statusline
Plug 'itchyny/lightline.vim'
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }
set laststatus=2

call plug#end()

colorscheme xcodedark
"colorscheme onehalfdark
"
